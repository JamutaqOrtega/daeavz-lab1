﻿using System;
using System.Windows.Forms;
using static Procedimientos_Funciones.Program;

namespace Procedimientos_Funciones_Form
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSumar_Click(object sender, EventArgs e)
        {
            txtResultado.Clear();
            int result = Suma(Convert.ToInt32(txtNum1.Text), Convert.ToInt32(txtNum2.Text));
            txtResultado.Text = result.ToString();
        }

        private void btnRestar_Click(object sender, EventArgs e)
        {
            txtResultado.Clear();
            int result = Resta(Convert.ToInt32(txtNum1.Text), Convert.ToInt32(txtNum2.Text));
            txtResultado.Text = result.ToString();
        }

        private void btnProducto_Click(object sender, EventArgs e)
        {
            txtResultado.Clear();
            int result = Multiplicacion(Convert.ToInt32(txtNum1.Text), Convert.ToInt32(txtNum2.Text));
            txtResultado.Text = result.ToString();
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            txtResultado.Clear();
            int result = Division(Convert.ToInt32(txtNum1.Text), Convert.ToInt32(txtNum2.Text));
            txtResultado.Text = result.ToString();
        }

        private void btnRaices_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            if (rbtnCaF.Checked)
            {
                txtResultadoTemp.Text = Conversion(2, Convert.ToInt32(txtEntrada.Text));
            }
            if (rbtnFaC.Checked)
            {
                txtResultadoTemp.Text = Conversion(1, Convert.ToInt32(txtEntrada.Text));
            }
        }
    }
}
