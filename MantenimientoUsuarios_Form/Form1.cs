﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MantenimientoUsuarios_Form
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //asigna los datos ingresados a variables
            string dni = txtDni.Text;
            string nombre = txtNombre.Text;
            string apellido = txtApellido.Text;
            string direccion = txtDireccion.Text;
            string telefono = txtTelefono.Text;
            string email = txtEmail.Text;
            string fecha_nac = dtpFechaNac.Text;
            bool activo = chbxActivo.Checked;

            string departamento = cbxDepartamento.Text;
            string cargo = cbxCargo.Text;

            //asigna las variables a una fila de la tabla
            dgvUsuarios.Rows.Add("", dni, nombre, apellido, direccion, telefono, email, fecha_nac, departamento, cargo);
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //obtiene todos los datos de las celdas seleccionadas
            var datos = dgvUsuarios.CurrentRow.Cells;

            //asigna dichos datos a los campos del formulario
            txtDni.Text = datos[1].Value.ToString();
            txtNombre.Text = datos[2].Value.ToString();
            txtApellido.Text = datos[3].Value.ToString();
            txtDireccion.Text = datos[4].Value.ToString();
            txtTelefono.Text = datos[5].Value.ToString();
            txtEmail.Text = datos[6].Value.ToString();
            dtpFechaNac.Text = datos[7].Value.ToString();

            cbxDepartamento.Text = datos[8].Value.ToString();
            cbxCargo.Text = datos[9].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtDni.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            txtEmail.Text = "";
            dtpFechaNac.Text = "";

            cbxDepartamento.Text = "";
            cbxCargo.Text = "";
        }
    }
}
