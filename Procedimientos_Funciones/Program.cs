﻿using System;
using System.Linq;

namespace Procedimientos_Funciones
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Procedimientos y funciones";

            string opcion;
            do
            {
                Console.Clear();
                Console.WriteLine("[1] Suma de dos números");
                Console.WriteLine("[2] Resta de dos números");
                Console.WriteLine("[3] Producto de dos números");
                Console.WriteLine("[4] División de dos números");
                Console.WriteLine("[5] Imprimir la raíz cuadrada de los 10 primeros números enteros");
                Console.WriteLine("[6] Imprimir los 10 primeros números primos");
                Console.WriteLine("[7] Convertir temperaturas");
                Console.WriteLine("[0] Salir");
                Console.Write(">_Ingrese una opción y presione ENTER: ");
                opcion = Console.ReadLine();

                switch (opcion)
                {
                    case "1":
                        Console.Write("\nIngrese el primer número: ");
                        int a = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Ingrese el segundo número: ");
                        int b = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine(">> {0} + {1} = {2}", a, b, Suma(a, b));
                        Console.ReadKey();
                        break;

                    case "2":
                        Console.Write("\nIngrese el primer número: ");
                        int c = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Ingrese el segundo número: ");
                        int d = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine(">> {0} - {1} = {2}", c, d, Resta(c, d));
                        Console.ReadKey();
                        break;

                    case "3":
                        Console.Write("\nIngrese el primer número: ");
                        int e = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Ingrese el segundo número: ");
                        int f = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine(">> {0} * {1} = {2}", e, f, Multiplicacion(e, f));
                        Console.ReadKey();
                        break;

                    case "4":
                        Console.Write("\nIngrese el primer número: ");
                        int g = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Ingrese el segundo número: ");
                        int h = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine(">> {0} / {1} = {2}", g, h, Division(g, h));
                        Console.ReadKey();
                        break;

                    case "5":
                        Console.WriteLine("\nCalculando...");
                        Raiz();
                        Console.ReadKey();
                        break;

                    case "6":
                        Console.WriteLine("\nCalculando...");
                        Primos();
                        Console.ReadKey();
                        break;

                    case "7":
                        Console.Write("\n1) Convertir de °F a °C\n2) Convertir de °C a °F\n>_Eliga una opción: ");
                        int op = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine(">_Ingrese la temperatura a convertir: ");
                        int temp = Convert.ToInt32(Console.ReadLine());
                        Conversion(op, temp);
                        Console.ReadKey();
                        break;
                }

            } while (!opcion.Equals("0"));

        }

        //FUNCION para calcular la suma de 2 numeros enteros
        public static int Suma(int a, int b)
        {
            return a + b;
        }
        //FUNCION para calcular la resta de 2 numeros enteros
        public static int Resta(int a, int b)
        {
            return a - b;
        }
        //FUNCION para calcular el producto de 2 numeros enteros
        public static int Multiplicacion(int a, int b)
        {
            return a * b;
        }
        //FUNCION para calcular la division de 2 numeros enteros
        public static int Division(int a, int b)
        {
            return a / b;
        }
        //FUNCION para convertir de C a F o F a C
        public static string Conversion(int opcion, int entrada)
        {
            string result = "";

            if (opcion == 1) //de F a C
            {
                result = $">> {entrada}°F = {(5 * (entrada - 32)) / 9}°C";
            }
            if (opcion == 2) //de C a F
            {
                result = $">> {entrada}°C = {((9 * entrada) / 5) + 32}°F";
            }

            return result;
        }


        //PROCEDIMIENTO que imprime la raiz cuadradada de los 23 primeros numeros
        static void Raiz()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Raíz cuadrada de {0} = {1}", i, Math.Sqrt(i));
            }
        }
        //PROCEDIMIENTO que imprime los números primos del 1 al 10
        static void Primos()
        {
            for (int i = 1; i <= 29; i++) //numeros del 1 al 29
            {
                int numero_divisores = 0;

                for (int j = 1; j <= i; j++) //verifica los divisores
                {
                    if (i % j == 0) //si el numero es divisible con j
                    {
                        numero_divisores++;
                    }
                }

                if (numero_divisores == 2) //un numero primo solo tiene 2 divisores
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
